import App from './components/App.vue';
import adduser from './components/user/AddUser.vue';
import userlist from './components/user/Userlist.vue';
import AddCategory from './components/category/add.vue';
import Categorylist from './components/category/list.vue';

export const routes = [
{
        name: 'home',
        path: '/home',
        component: App
    },
    {
        name: 'adduser',
        path: '/add-user',
        component: adduser
    },
    {
        name: 'userlist',
        path: '/user-list',
        component: userlist
    },
    {
        name: 'categorylist',
        path: '/category-list',
        component: Categorylist
    },
    {
        name: 'addcategory',
        path: '/add-category',
        component: AddCategory
    },
];