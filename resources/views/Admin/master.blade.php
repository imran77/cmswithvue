@include('Admin.common.header')
@include('Admin.common.sidebar')

    <!-- Main content -->
        @yield('main_content')
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->
@include('Admin.common.footer')