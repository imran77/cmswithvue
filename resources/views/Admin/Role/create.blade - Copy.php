@extends('Admin.master')
@section('main_content')
<div class="inner-wrapper">
<section role="main" class="content-body">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Create New Role</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif


{!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
    <br>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
    <br>
            <strong>Permission:</strong>
                
            <ul style="list-style: none">
        

            @foreach($permission as $value)
                <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                {{ $value->name }}</label>
            <br/>
            @endforeach
            </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{!! Form::close() !!}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
{{-- 
<script type="text/javascript">
    $(document).ready(function(){
$(".chk").click(function(){
    // alert('sdd');
var d = $("input[name=permission]"); if(d.is(":checked")){
//
}
});
});


</script> --}}

<script language="javascript">
// $(function(){

//     // add multiple select / deselect functionality
//     $(".selectall").click(function () {
//           $('.case').attr('checked', this.checked);
//     });

//     // if all checkbox are selected, check the selectall checkbox
//     // and viceversa
//     $(".case").click(function(){

//         if($(".case").length == $(".case:checked").length) {
         
//             $(".selectall").prop('checked', true);
//         } else {
//             $(".selectall").prop('checked', false);
//             // $(".selectall").removeAttr("checked");
//         }

//     });
// });

 $(".selectall").click(function () {
    // $(this).closest('.a');
     $('input:checkbox').not(this).closest('.case').prop('checked', this.checked);
 });
</script>


@endsection