@extends('Admin.master')
@section('main_content')
<div class="inner-wrapper">
<section role="main" class="content-body">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add Role</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif


{!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
    <br>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
    <br>
            <strong>General Permission:</strong> 
                                     
            <ul style="list-style: none" class="permissions">
 
                            <a href="#" class="permission-select-all">Select All</a> / <a href="#"  class="permission-deselect-all">Deselect All</a>
                           
            @foreach($general_permissions as $list)

       <li>
                <label>
                    {{-- {{ Form::checkbox('', $list->permission_id, false, array('class' => 'permission-group')) }} --}}
                     <strong>{{ ucwords($list->table_name) }}</strong>
{{-- 
                        <input type="checkbox" id="permission-{{$list->table_name}}-sub" class="permission-group"> --}}
                 </label>
                 
            @foreach($permission as $value)

                &nbsp;&nbsp; &nbsp;&nbsp;
                 @if(empty($value->table_name))
                <label>
                     {{ $value->name }}
                     <input type="checkbox" id="permission-{{$value->id}}" name="permissions[]" class="the-permission" value="{{$value->id}}" data-table_name="{{$value->table_name}}">
                 </label>
                 @endif
            <br/>
            
            @endforeach
        
       </li>
            @endforeach
            </ul> 
            <strong>Permission:</strong>               
            <hr>               
            <ul style="list-style: none" class="permissions">
 
            @foreach($table_lists as $list)

       <li>
                @if(!empty($list->table_name))
                <label>
                    {{-- {{ Form::checkbox('', $list->permission_id, false, array('class' => 'permission-group')) }} --}}
                     <strong>{{ ucwords($list->table_name) }}</strong>
{{-- 
                        <input type="checkbox" id="permission-{{$list->table_name}}-sub" class="permission-group"> --}}
                 </label>
                 <br/>
            @foreach($permission as $value)

            @if(!empty($list->table_name == $value->table_name))
                
                &nbsp;&nbsp; &nbsp;&nbsp;
                <label>
                    {{-- {{ Form::checkbox('permission[]', $value->id, false, array('class' => 'the-permission')) }} --}}
                     {{ $value->name }}
                     <input type="checkbox" id="permission-{{$value->id}}" name="permissions[]" class="the-permission" value="{{$value->id}}" data-table_name="{{$value->table_name}}">
                 </label>
            <br/>
            @endif
            @endforeach
            @endif
       </li>
            @endforeach
            </ul>


    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{!! Form::close() !!}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
        $('document').ready(function () {
            // $('.toggleswitch').bootstrapToggle();

            // $('.permission-group').on('click', function(){
            //     // alert();
            //     $('.the-permission').find("input[type='checkbox']").prop('checked', true);
            // });

            $('.permission-select-all').on('click', function(){
                $('ul.permissions').find("input[type='checkbox']").prop('checked', true);
                return false;
            });

            $('.permission-deselect-all').on('click', function(){
                $('ul.permissions').find("input[type='checkbox']").prop('checked', false);
                return false;
            });

            // function parentChecked(){
            //     $('.the-permission').each(function(){
            //         var allChecked = true;
            //         $(this).siblings('ul').find("input[type='checkbox']").each(function(){
            //             if(!this.checked) allChecked = false;
            //         });
            //         $(this).prop('checked', allChecked);
            //     });
            // }

            // parentChecked();


            // $('.the-permission').on('change', function(){
            //     parentChecked();
            // });

            // $(document).on('click', '.the-permission', function () {
            //     let group_id = $(this).data('group');
            //     let table_name = $(this).data('table_name');
            //     // alert(group_id);

            //     if (group_id ==table_name ) {

            //     // $(".the-permission").find("input[type='checkbox']").prop('checked',true);
            //      $("#"+group_id).find("input[type='checkbox']").prop('checked', true);
            //     }


            // });


        });
    </script>


@endsection