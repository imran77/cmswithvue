<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

	<div class="sidebar-header">
		{{-- <div class="sidebar-title">
			Navigation
		</div> --}}
		<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
			<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<div class="nano">
		<div class="nano-content">
			<nav id="menu" class="nav-main" role="navigation">
				<ul class="nav nav-main">
					<li class="nav-active">
						<a href="{{url('/dashboard')}}">
							<i class="fa fa-home" aria-hidden="true"></i>
							<span>Dashboard</span>
						</a>
					</li>

					<li class="nav-parent">
						<a>
							<i class="fa fa-user" aria-hidden="true"></i>
							<span>User</span>
						</a>
						<ul class="nav nav-children">

							{{-- 	<a href="{{route('users.create')}}">
									 Add User
								</a> --}}
								 <router-link :to="{ name: 'add-user' }" class="{{ (request()->is('page')) ? 'nav-item  active' : '' }}">Add User</router-link>
						

							<li>
								<a href="{{route('users.index')}}">
									 User List
								</a>
							</li>
						
						</ul>
					</li>

					<li class="nav-parent">
						<a>
							<i class="fa fa-user" aria-hidden="true"></i>
							<span>Permission Manage</span>
						</a>
						<ul class="nav nav-children">

							<li class="{{ (request()->is('permissions')) ? 'nav-item  active' : '' }}">
								<a href="{{route('permissions.create')}}">
									 Add Permission
								</a>
							</li>

							<li>
								<a href="{{route('permissions.index')}}">
									  Permission List
								</a>
							</li>
						
						</ul>
					</li>
					<li class="nav-parent">
						<a>
							<i class="fa fa-user" aria-hidden="true"></i>
							<span>Role Manage</span>
						</a>
						<ul class="nav nav-children">

							<li class="{{ (request()->is('page')) ? 'nav-item  active' : '' }}">
								<a href="{{route('roles.create')}}">
									 Add Role
								</a>
							</li>

							<li>
								<a href="{{route('roles.index')}}">
									 Role List
								</a>
							</li>
						
						</ul>
					</li>

					<li class="nav-parent {{ (request()->is('page/create*')) ||  (request()->is('page/create*'))  ? 'active menu-open' : '' }}">
						<a>
							<i class="fa fa-copy" aria-hidden="true"></i>
							<span>Pages</span>
						</a>
						<ul class="nav nav-children">

							<li class="nav-item {{ (request()->is('page/create')) ||  (request()->is('page/create*'))  ? 'active menu-open' : '' }}">
								<a href="{{ route('page.create')}}" class="{{ (request()->is('page/create')) ? 'nav-item  nav-active' : '' }}">
									<i class="fa fa-plus"></i>
									 Add Page
								</a>
							</li>

							<li>
								<a href="{{route('page.index')}}">
									<i class="fa fa-list"></i> Page List
								</a>
							</li>

						
						</ul>
					</li>
					<li class="nav-parent">
						<a>
							<i class="fa fa-tasks" aria-hidden="true"></i>
							<span>Category</span>
						</a>
						<ul class="nav nav-children">
							<li>
								<a href="{{route('category.create')}}">
									<i class="fa fa-plus"></i> Add Category
								</a>
							</li>
							<li>
								<a href="{{route('category.index')}}">
									<i class="fa fa-list"></i> Category List
								</a>
							</li>
							
						</ul>
					</li>
					<li class="nav-parent">
						<a>
							<i class="fa fa-list-alt" aria-hidden="true"></i>
							<span>Post</span>
						</a>
						<ul class="nav nav-children">
							<li>
								<a href="{{route('post.create')}}">
									 Add Post
								</a>
							</li>
							<li>
								<a href="{{route('post.index')}}">
									  Post List
								</a>
							</li>
						</ul>
					</li>
	
					
					<li class="nav-parent">
						<a>
							<i class="fa fa-table" aria-hidden="true"></i>
							<span>Menu</span>
						</a>
						<ul class="nav nav-children">
							<li>
								<a href="tables-basic.html">
									 Basic
								</a>
							</li>
							<li>
								<a href="tables-advanced.html">
									 Advanced
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-parent">
						<a>
							<i class="fa fa-table" aria-hidden="true"></i>
							<span>Tag</span>
						</a>
						<ul class="nav nav-children">
							<li>
								<a href="tables-basic.html">
									 Basic
								</a>
							</li>
							<li>
								<a href="tables-advanced.html">
									 Advanced
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a>
							<i class="fa fa-table" aria-hidden="true"></i>
							<span>Settings</span>
						</a>
					</li>
				</ul>
			</nav>

			<hr class="separator" />

		</div>

	</div>

</aside>
<!-- end: sidebar -->